<?php
require_once 'model/Product.php';


class ProductFormValidation {
    
    /**
     * validates POST data from product form.
     * @return object Product  with the given data or null if data is not present and valid.
     */
    public static function getProductData() {
        $productObj = null;
        $code = "";
        //retrieve code sent by client.
        if (filter_has_var(INPUT_POST, 'code')) {
            $code = filter_input(INPUT_POST, 'code'); 
        }
        $description = "";
        //retrieve description sent by client.
        if (filter_has_var(INPUT_POST, 'description')) {
            $description = filter_input(INPUT_POST, 'description'); 
        }
        $price = 0.0;
        //retrieve price sent by client.
        if (filter_has_var(INPUT_POST, 'price')) {
            $price = filter_input(INPUT_POST, 'price',FILTER_SANITIZE_NUMBER_FLOAT); 
        }
        
        if (!empty($code) && !empty($description) && !empty($price)) { 
        //they exists and they are not empty
            $productObj  = new Product(0, $code, $description, $price);
        }
        return $productObj;
    }
    
}
