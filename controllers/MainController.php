<?php
require_once 'lib/ViewLoader.php';
require_once 'model/Model.php';
require_once 'lib/ProductFormValidation.php';
require_once 'model/OrderItem.php';
require_once 'model/Order.php';

/**
 * Main controller of item application.
 */
class MainController
{

    private $view;

    private $Model;

    private $action;

    public function __construct()
    {
        //instantiate the view loader.
        $this->view = new ViewLoader();
        //instantiate the model.
        $this->Model = new Model();
    }

    /**
     * process requests from client, regarding action command.
     */
    public function processRequest()
    {
        $requestMethod = filter_input(INPUT_SERVER, 'REQUEST_METHOD');
        switch ($requestMethod) {
            case 'get':
            case 'GET':
                $this->processGet();
                break;
            case 'post':
            case 'POST':
                $this->processPost();
                break;
            default:
                break;
        }
    }

    private function processGet()
    {
        $this->action = "";
        if (filter_has_var(INPUT_GET, 'action')) {
            $this->action = filter_input(INPUT_GET, 'action');
        }
        switch ($this->action) {
            case 'home':
                $this->home();
                break;
            case 'login':
                $this->showLoginForm();
                break;
            case 'logout':
                $this->logout();
                break;
            case 'products/listProducts':
                $action = $this->action;
                $this->listAllProducts($action);
                break;
            case 'products/manageProducts':
                $action = $this->action;
                $this->manageProducts($action);
                break;
            case 'products/addProduct':

                $this->addProduct();
                break;
            case 'products/deleteProduct':
                $this->deleteProductPage();
                break;
            case 'products/modifyProduct':
                $this->modifyProductPage();
                break;

            case 'orders/shoppingCart':
                $this->shoppingCartPage();
                break;
            case 'orders/createOrder':
                $this->createOrderPage();
                break;
            case 'orders/listMyOrders':
                $this->listMyOrders();
                break;
            case 'orders/listOrders':
                $this->listOrders();
                break;






            default:  //processing default action.
                $this->home();
                break;
        }

    }

    private function processPost()
    {
        $this->action = "";
        if (filter_has_var(INPUT_POST, 'action')) {
            $this->action = filter_input(INPUT_POST, 'action');
        }
        //print_r($this->action);
        //echo "<br>";
        //echo "<br>";
        switch ($this->action) {

            case 'doLogin':
                $this->doLogin();
                break;
            case 'doLogout':
                $this->doLogout();
                break;
            case 'products/filterProducts':
                $action = $this->action;
                $this->filterProducts($action);
                break;
            case 'products/validateAddProduct':

                $this->validateAddProduct();
                break;
            case 'products/doDelete':
                //var_dump($_POST);
                $this->validateDeleteProduct();
                break;
            case 'products/validateModifyProduct':
                //var_dump($_POST);
                $this->validateModifyProduct();
                break;
            case 'orders/searchProducts':
                //var_dump($_POST);
                $action = $this->action;

                $this->searchProducts($action);
                break;

            case 'orders/addProductToOrder':
                //var_dump($_POST);
                //print_r($_POST);
                $this->addProductToOrder();

                break;

            case 'orders/confirmDeleteShoppingCart':
                $this->confirmDeleteShoppingCart();
                break;
            case 'orders/deleteShoppingCart':
                $this->deleteShoppingCart();
                break;
            case 'orders/createOrderInDatabase':
                //var_dump($_POST);
                $this->createOrderInDatabase();
                break;
            default:  //processing default action.
                $this->home();
                break;
        }

    }





    /**
     * displays home page content.
     */
    public function home()
    {
        $this->view->show("home.php", null);
    }


    /**
     * displays login page content.
     */
    public function showLoginForm()
    {
        $this->view->show("login.php", null);
    }


    /**
     * prosesses login.
     */
    public function doLogin()
    {
        $username = filter_input(INPUT_POST, 'username');
        $password = filter_input(INPUT_POST, 'password');
        //echo $username;
        //echo $password;

        $user = $this->Model->searchUsernamePassword($username, $password);


        if ($user != null) {
            $data['user'] = $user;

            // save data to session
            $_SESSION["username"] = $user->getUsername();
            $_SESSION['role'] = $user->getRole();
            $_SESSION['email'] = $user->getEmail();
            $_SESSION["status"] = true;

            header("Location: index.php?action=home");
        } else {
            $data['result'] = "Invalid credentials";
            $data['password'] = $password;
            $data['username'] = $username;
            $this->view->show("login.php", $data);

        }

    }



    /**
     * Destroys the session and redirects to the home page.
     */
    public function logout()
    {
        //$this->view->show("logout.php", null);
        session_destroy();
        $_SESSION = array();
        //$this->myView->show("home.php", null); //redirect to index.php
        header("Location: index.php?action=home");
        exit;

    }

    /**
     * Destroys the session and redirects to the home page.
     */
    /*     public function doLogout()
        {
            session_destroy();
            $_SESSION = array();
            //$this->myView->show("home.php", null); //redirect to index.php
            header("Location: index.php?action=home");
            exit;
        } */

    /**
     * Shows all products
     */
    public function listAllProducts($action)
    {
        $this->Model->selectAllProducts();
        $data['products'] = $this->Model->selectAllProducts();
        $data['action'] = $action;
        $this->view->show("products/listAllProducts.php", $data);
    }



    /**shows manage products page
     */
    public function manageProducts($action)
    {
        $this->Model->selectAllProducts();
        $data['products'] = $this->Model->selectAllProducts();
        $data['action'] = $action;
        $this->view->show("products/manageProducts.php", $data);
    }


    /**shows manage products page with filtered products
     */
    public function filterProducts($action)
    {
        $descriptionToSearch = filter_input(INPUT_POST, 'descriptionInput');
        $data['filteredProducts'] = $this->Model->searchByDescription($descriptionToSearch);
        $data['action'] = $action;
        $this->view->show("products/listAllProducts.php", $data);
    }

    /**shows create order page with filtered products
     */
    public function searchProducts($action)
    {
        $descriptionToSearch = filter_input(INPUT_POST, 'descriptionInput');
        $data['filteredProducts'] = $this->Model->searchByDescription($descriptionToSearch);
        $data['action'] = $action;
        //var_dump($data['filteredProducts'] );
        $this->view->show("orders/createOrder.php", $data);
    }
    /** shows adds product   page  */
    public function addProduct()
    {

        $this->view->show("products/addProduct.php", null);
    }

    /** processes add product action  */
    public function validateAddProduct()
    {
        $product = ProductFormValidation::getProductData();
        //var_dump($product);
        if ($product != null) {
            $result = $this->Model->addProduct($product);
            //echo($result);
            if ($result == 1) { //success
                $data['productAdded'] = $product->getId();
                $data['message'] = "Added product with ID ";
                $data['action'] = "products/manageProducts";
                $data['products'] = $this->Model->selectAllProducts();
                $this->view->show("products/manageProducts.php", $data);
            } elseif ($result == 2) {  //in case the code exists
                //$data['productAdded'] = $product->getId();
                $data['message'] = "Product not added.The entered product code already exists.";
                $data['action'] = "products/manageProducts";
                $data['description'] = filter_input(INPUT_POST, 'description');
                $data['price'] = filter_input(INPUT_POST, 'price');
                $data['products'] = $this->Model->selectAllProducts();
                $this->view->show("products/addProduct.php", $data);
            } else {  //problems with database
                $data['message'] = "Product not added.Database error";
                $this->view->show("products/addProduct.php", $data);
            }
        } else { //price is not numeric
            $data['message'] = "Price is not correct";
            $data['description'] = filter_input(INPUT_POST, 'description');
            $data['code'] = filter_input(INPUT_POST, 'code');

            $this->view->show("products/addProduct.php", $data);

        }
        //$this->view->show("products/addProduct.php", null);
    }

    /** shows delete product   page  */
    private function deleteProductPage()
    {
        $id = filter_input(INPUT_GET, 'id');
        //var_dump($_GET);
        //echo "<br>";
        //echo "<br>";

        $product = $this->Model->searchProductById($id);
        //var_dump($product);
        //echo "<br>";
        //echo "<br>";
        $data['product'] = $product;
        $this->view->show("products/deleteProduct.php", $data);

    }


    /** processes delete product action  */
    public function validateDeleteProduct()
    {

        //var_dump($_POST);
        $id = filter_input(INPUT_POST, 'id');
        $result = $this->Model->deleteProduct($id);
        //echo($result);
        if ($result == 1) {
            $data['productDeletedId'] = $id;
            $data['message'] = "Deleted product with ID ";
            $data['action'] = "products/manageProducts";
            $data['products'] = $this->Model->selectAllProducts();
            //print_r($data['action']); 
            $this->view->show("products/manageProducts.php", $data);
        } else {
            $data['message'] = "Product not deleted, database error";
            $data['action'] = "products/manageProducts";
            $data['products'] = $this->Model->selectAllProducts();
            $this->view->show("products/manageProducts.php", $data);
        }

    }

    /** shows modify product   page  */
    private function modifyProductPage()
    {
        $id = filter_input(INPUT_GET, 'id');
        //var_dump($_GET);
        //echo "<br>";
        //echo "<br>";

        $product = $this->Model->searchProductById($id);
        //var_dump($product);
        //echo "<br>";
        //echo "<br>";
        $data['product'] = $product;
        $this->view->show("products/modifyProduct.php", $data);

    }

    /** processes modify product action  */
    public function validateModifyProductOld()
    {
        //echo "en validate modify";
        //echo "<br>";
        //echo "<br>";
        //var_dump($_POST);
        //echo "<br>";
        //echo "<br>";
        $description = filter_input(INPUT_POST, 'description');
        $price = filter_input(INPUT_POST, 'price');
        $code = filter_input(INPUT_POST, 'code');
        $id = filter_input(INPUT_POST, 'id');

        $product = $this->Model->searchProductById($id);

        if (!is_numeric($price)) {
            $data['message'] = "Price is not correct";
            $data['product'] = $product;
            $data['description'] = $description;
            $this->view->show("products/modifyProduct.php", $data);
        } else {
            $updatedProduct = new Product($id, $code, $description, $price);
            //echo "update product";
            //echo "<br>";
            //echo "<br>";
            //var_dump($updatedProduct);
            //echo "<br>";
            //echo "<br>";
            $result = $this->Model->updateProduct($updatedProduct);
            //echo $result;

            if ($result == 1) {
                $data['productModified'] = $updatedProduct->getId();
                $data['message'] = "Has been modified product with code ";
                $data['action'] = "products/manageProducts";
                $data['products'] = $this->Model->selectAllProducts();
                $this->view->show("products/manageProducts.php", $data);
            } else {
                $data['message'] = "Product not modified, database error";
                $this->view->show("products/modifyProduct.php", $data);
            }
        }

    }



    /** processes modify product action  */
    public function validateModifyProduct()
    {
        //echo "en validate modify";
        //echo "<br>";
        //echo "<br>";
        //var_dump($_POST);
        //echo "<br>";
        //echo "<br>";
        $description = filter_input(INPUT_POST, 'description');
        $price = filter_input(INPUT_POST, 'price');
        $code = filter_input(INPUT_POST, 'code');
        $id = filter_input(INPUT_POST, 'id');
        $product = $this->Model->searchProductById($id);

        $validateData = $this->Model->validateModifyProductData($id, $code, $description, $price);
        //echo($validateData);

        switch ($validateData) {
            case 1:
                // All data is valid
                $updatedProduct = new Product($id, $code, $description, $price);
                $result = $this->Model->modifyProduct($updatedProduct);

                if ($result == 1) {
                    $data['productModified'] = $updatedProduct->getId();
                    $data['message'] = "Has been modified product with code ";
                    $data['action'] = "products/manageProducts";
                    $data['products'] = $this->Model->selectAllProducts();
                    $this->view->show("products/manageProducts.php", $data);
                } else {
                    $data['message'] = "Product not modified, database error";
                    $this->view->show("products/modifyProduct.php", $data);
                }
                break;

            case 2:
                // Code already exists
                $data['message'] = "Code already exists";
                $data['description'] = $description;
                $data['product'] = $product;
                $data['price'] = $price;
                $this->view->show("products/modifyProduct.php", $data);
                break;

            case 3:
                // Price is not numeric
                $data['message'] = "Price is not numeric";
                $data['description'] = $description;
                $data['code'] = $code;
                $data['product'] = $product;
                $this->view->show("products/modifyProduct.php", $data);
                break;

            case 4:
                // Price is not valid
                $data['message'] = "Price is not valid";
                $data['code'] = $code;
                $data['description'] = $description;
                $data['product'] = $product;
                $this->view->show("products/modifyProduct.php", $data);
                break;

            case 5:
                // Code and description are not valid
                $data['message'] = "Code and description are not valid";
                $data['price'] = $price;
                $data['product'] = $product;
                $this->view->show("products/modifyProduct.php", $data);
                break;

            case 6:
                // Description and price are not valid
                $data['message'] = "Description and price are not valid";
                $data['code'] = $code;
                $data['product'] = $product;
                $this->view->show("products/modifyProduct.php", $data);
                break;

            case 7:
                // Price and code are not valid
                $data['message'] = "Price and code are not valid";
                $data['description'] = $description;
                $data['product'] = $product;
                $data['product'] = $product;
                $this->view->show("products/modifyProduct.php", $data);
                break;

            case 8:
                // None of the data is valid
                $data['message'] = "None of the data is valid";
                $data['product'] = $product;
                $this->view->show("products/modifyProduct.php", $data);
                break;

            default:
                // Handle other cases if needed
                break;
        }



    }

    /**
     * Display the shopping cart page.
     * Retrieves user information and order items from the session,
     * groups order items by product ID, and renders the shopping cart page.
     */

    public function shoppingCartPage()
    {
        $user = $this->Model->searchUsername($_SESSION['username']);
        //print_r($user);
        $data['user'] = $user;
        if (isset($_SESSION["orderItems"]) && !empty($_SESSION["orderItems"])) {
            //print_r($_SESSION["orderItems"]);
            $items = $_SESSION["orderItems"];
            $groupedItems = $this->Model->groupOrderItemsByProductId($items);
            $data['orderItems'] = $groupedItems;
            //$data['orderItems'] = $_SESSION["orderItems"];

        }
        $this->view->show("orders/shoppingCart.php", $data);
    }
    /**
 * Display the create order page.

 */


    public function createOrderPage()
    {
        $this->view->show("orders/createOrder.php", null);
    }


    /**
     * Add a product to the order.
     * Validates the quantity, creates an OrderItem object,
     * adds it to the session's order items array, and redirects to the shopping cart page.
     */

    public function addProductToOrder()
    {
        $quantity = filter_input(INPUT_POST, 'productQuantity', FILTER_VALIDATE_INT);
        //echo($quantity);
        if (($quantity === false || $quantity <= 0)) {
            $data['message'] = "Quantity should be numeric and more than 0";
            $this->view->show("orders/createOrder.php", $data);
        } else {
            $itemPrice = filter_input(INPUT_POST, 'price');
            $productId = filter_input(INPUT_POST, 'id');

            $orderItems = isset($_SESSION["orderItems"]) ? $_SESSION["orderItems"] : [];

            $itemToOrder = new OrderItem(null, $productId, $quantity, $itemPrice);
            array_push($orderItems, $itemToOrder);


            $_SESSION["orderItems"] = $orderItems;
            echo ("session");

            //var_dump($_SESSION["orderItems"]);
            header("Location: index.php?action=orders/shoppingCart");
        }

    }
    /**
     * Display the confirmation page for deleting the shopping cart.
     */

    public function confirmDeleteShoppingCart()
    {
        $this->view->show("orders/confirmDeleteShoppingCart.php", null);

    }
    /**
     * Delete the shopping cart.
     * Unsets the order items from the session and redirects to the home page.
     */

    public function deleteShoppingCart()
    {
        unset($_SESSION["orderItems"]);
        header("Location: index.php?action=home");
        exit;

    }
    /**
     * Create an order in the database.
     * Retrieves order items from the session, groups them by product ID,
     * retrieves the delivery method and customer ID from the POST data,
     * and creates an order with items in the database.
     * If successful, unsets the order items from the session and displays a success message with the order number.
     */

    public function createOrderInDatabase()
    {
        $items = $_SESSION["orderItems"];
        $groupedItems = $this->Model->groupOrderItemsByProductId($items);
        //echo ("<br>");
        //echo ("<br>");
        //print_r($groupedItems);

        //var_dump($_POST);
        $deliveryMethod = filter_input(INPUT_POST, 'select');
        $user = $this->Model->searchUsername($_SESSION['username']);
        $customerId = $user->getId();
        //echo ("<br>");
        //echo($deliveryMethod);
        //echo ("<br>");
        //echo($customerId);
        $result = $this->Model->createOrderWithItems($customerId, $deliveryMethod, $groupedItems);
        //echo($result);
        if ($result > 0) {
            unset($_SESSION["orderItems"]);
        }
        $data['message'] = "Thank you for your order! Your order number is " . $result;
        //print_r($data);
        $this->view->show("orders/orderNumber.php", $data);
    }



    /*    List the orders associated with the currently logged-in user.
       Retrieves the user ID of the logged-in user from the session,
       then retrieves the orders associated with that user from the database.
       If orders are found, they are passed to the view for display.
       If no orders are found, a message indicating no orders are present is displayed. */
    public function listMyOrders()
    {
        $user = $this->Model->searchUsername($_SESSION['username']);
        //print_r($user);
        $userId = $user->getId();
        //echo($userId);
        $orders = $this->Model->searchUserOrders($userId);
        //print_r($orders);

        if (!empty($orders)) {
            $data['orders'] = $orders;
            $this->view->show("orders/myOrders.php", $data);
        } else {
            $data["message"] = "You have no orders";
            $this->view->show("orders/myOrders.php", $data);
        }


    }

    /**
     * List all orders in the system.
     * Retrieves the ID of the currently logged-in user from the session, 
     * then retrieves all orders from the database.
     * If orders are found, they are passed to the view for display.
     * If no orders are found, a message indicating no orders are present is displayed.
     */

    public function listOrders()
    {
        $user = $this->Model->searchUsername($_SESSION['username']);
        //print_r($user);
        $userId = $user->getId();
        //echo($userId);
        $orders = $this->Model->searchAllOrders();
        //print_r($orders);

        if (!empty($orders)) {
            $data['orders'] = $orders;
            $this->view->show("orders/showAllOrders.php", $data);
        } else {
            $data["message"] = "there are no orders";
            $this->view->show("orders/showAllOrders.php", $data);
        }


    }


}


