<?php

if (isset($_SESSION["status"]) && $_SESSION['role'] == "admin")  {
    //echo("en delete product page");
    $product = $params['product'] ?? null;
    //var_dump($product);
    echo <<<EOT
   
    
    <form action="index.php" method="post">
    <div class="row gy-3 gy-md-4 ms-5 overflow-hidden">
      <div class="col-3">
      
      <p>Are you sure you want to delete product with ID {$product->getId()}?</p>
      <input type="hidden" name="id" value="{$product->getId()}">
     
      <button type="submit" class="btn bsb-btn-xl btn-light" id="button" name = "action" value="products/doDelete">Delete</button>

    </div>  
    </div>  
  </form>
  <br>
  <br>
EOT;
}
else {
echo ("Access denied");
}
