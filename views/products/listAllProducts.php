<?php //echo ("en list all products");
$products = $params['products'] ?? null;
$filteredProducts = $params['filteredProducts'] ?? null;
$action = $params['action'];
/* var_dump($products);
echo ("<br>");
echo ("<br>");
var_dump($filteredProducts);
echo ("<br>");
echo ("<br>");
var_dump($_POST); */

echo <<<EOT


<form action="index.php" method="post">
<div class="row gy-3 gy-md-4 ms-5 overflow-hidden">
  <div class="col-3">
    <label for="descriptionInput" class="form-label">Product search</label>
    <input type="text" class="form-control" name="descriptionInput" id="descriptionInput"  maxlength="10"  required>
  </div>  
  <div class="col-3">
    <div class="d-grid">
<p></p><p></p>
      <button class="btn bsb-btn-xl btn-light" type="submit" id="button" name = "action" value="products/filterProducts">search</button>
    </div>
  </div>
</div>
<div class="row gy-3 gy-md-4 overflow-hidden">

  </div>
</form>
<br>
<br>

<div class="row gy-3 gy-md-4 ms-5 me-5 overflow-hidden">
<table class="table ">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Code</th>
      <th scope="col">Description</th>
      <th scope="col">Price</th>
     

    </tr>
  </thead>
  <tbody>

EOT;
if ($action == "products/listProducts") {
    if (!empty($products)) {
        foreach ($products as $product) {
            echo <<<EOT
    <tr>
        <td scope="row">{$product->getId()}</td>
        <td>{$product->getCode()}</a></td>
        <td>{$product->getDescription()}</td>
        <td>{$product->getPrice()}</td>
        
     </tr>  
EOT;
        }
    } else {
        echo '<tr><td colspan="4">products not found</td></tr>';
    }
}

if ($action == "products/filterProducts") {
    if (!empty($filteredProducts)) {
        foreach ($filteredProducts as $product) {
            echo <<<EOT
    <tr>
        <td scope="row">{$product->getId()}</td>
        <td>{$product->getCode()}</a></td>
        <td>{$product->getDescription()}</td>
        <td>{$product->getPrice()}</td>
        
    </tr>  
EOT;
        }
    } else {
        echo '<tr><td colspan="4">products not found</td></tr>';
    }
}
echo <<<EOT

</tbody>
</table>
</div>
EOT;
