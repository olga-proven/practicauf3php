<?php //echo ("en add product");



if (isset($_SESSION["status"]) && $_SESSION['role'] == "admin") {


  $message = $params['message'] ?? null;
  $code = $params['code'] ?? null;
  $description = $params['description'] ?? null;
  $price = $params['price'] ?? null;

  //$filteredProducts = $params['filteredProducts'] ?? null;
//$action = $params['action'];
/* var_dump($products);
echo ("<br>");
echo ("<br>");
var_dump($filteredProducts);
echo ("<br>");
echo ("<br>");
var_dump($_POST); */

  echo <<<EOT
<form action="index.php" method="post">
<div class="row gy-3 gy-md-4 ms-5 overflow-hidden">
  <div class="col-3">
    <label for="code" class="form-label">Product code</label>

EOT;
  if (!empty($code)) {

    echo <<<EOT

                <input type="text" class="form-control" name="code" id="code" value="{$code}"required>
                EOT;
  } else {
    echo <<<EOT
        <input type="text" class="form-control" name="code" id="code"  maxlength="10"  required>
    EOT;
  }
  echo <<<EOT
    
    <label for="description" class="form-label">Description:</label>
    EOT;



  if (!empty($description)) {

    echo <<<EOT
  
                  <input type="text" class="form-control" name="description" id="description"  value="{$description}" maxlength="100"  required>
                  EOT;
  } else {
    echo <<<EOT
      <input type="text" class="form-control" name="description" id="description"  maxlength="100"  required>
      EOT;
  }
  echo <<<EOT


    
  
    <label for="price" class="form-label">Price:</label>
    EOT;

  if (!empty($price)) {

    echo <<<EOT
  
                  <input type="text" class="form-control mb-2" name="price" value="{$price}"id="price"  required>
                  EOT;
  } else {
    echo <<<EOT
      <input type="text" class="form-control mb-2" name="price"  id="price"  required>
      EOT;
  }
  echo <<<EOT



    
    <button class="btn bsb-btn-xl btn-light mb-2" type="submit" id="button" name = "action" value="products/validateAddProduct">Add</button>
    <a href="index.php?action=products/manageProducts" class="btn bsb-btn-xl btn-light mb-2">Cancel</a>
    <p class="text-decoration-underline text-danger" >$message</p>
  
    
  </div>  
  </div>  
</form>
<br>
<br>
EOT;
} else {
  echo ("Access denied");
}
