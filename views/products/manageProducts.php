<?php //echo ("en list all products");
$products = $params['products'] ?? null;
$filteredProducts = $params['filteredProducts'] ?? null;
$action = $params['action'];
$productAdded = $params['productAdded'] ?? null;
$productDeletedId = $params['productDeletedId'] ?? null;
$productModified = $params['productModified'] ?? null;
$message = $params['message'] ?? null;

if (isset($_SESSION["status"]) && $_SESSION['role'] == "admin") {

/* var_dump($products);
echo ("<br>");
echo ("<br>");
var_dump($filteredProducts);
echo ("<br>");
echo ("<br>");
var_dump($_POST); */

if ($productAdded != null) {
  echo <<<EOT
<p class="text-decoration-underline text-danger">$message $productAdded</p>
EOT;
}
if ($productDeletedId != null) {
  echo <<<EOT
  <p class="text-decoration-underline text-danger">$message $productDeletedId</p>
  EOT;
}

if ($productModified != null) {
  echo <<<EOT
  <p class="text-decoration-underline text-danger">$message $productModified</p>
  EOT;
}

echo <<<EOT



<form action="index.php" method="post">

<div class="row gy-3 gy-md-4 ms-5 overflow-hidden">
  <div class="col-3">
    <label for="descriptionInput" class="form-label">Product search</label>
    <input type="text" class="form-control" name="descriptionInput" id="descriptionInput"  maxlength="10"  required>
  </div>  
  <div class="col-3">
    <div class="d-grid">
<p></p><p></p>
      <button class="btn bsb-btn-xl btn-light" type="submit" id="button" name = "action" value="products/filterProducts">search</button>
    </div>
  </div>
</div>
<div class="row gy-3 gy-md-4 overflow-hidden">

  </div>
</form>
<br>
<br>


<div class="row gy-3 gy-md-4 ms-5 mb-3 overflow-hidden">
  <div class="col-3">
   
    <a href="index.php?action=products/addProduct" class="btn bsb-btn-xl btn-light">Add product</a>
  </div>  
  
</div>


<div class="row gy-3 gy-md-4 ms-5 overflow-hidden">
<table class="table">
  <thead>
  

    <tr>
      <th scope="col">Id</th>
      <th scope="col">Code</th>
      <th scope="col">Description</th>
      <th scope="col">Price</th>
      <th scope="col">Delete</th>
      <th scope="col">Modify</th>

    </tr>
  </thead>
  <tbody>

EOT;
if ($action == "products/manageProducts") {
  if (!empty($products)) {
    foreach ($products as $product) {
      echo <<<EOT
    <tr>
        <td scope="row">{$product->getId()}</td>
        <td>{$product->getCode()}</a></td>
        <td>{$product->getDescription()}</td>
        <td>{$product->getPrice()}</td>
        <td><a href="index.php?id={$product->getId()}&action=products/deleteProduct" class="btn bsb-btn-xl btn-light">Delete product</a></td>
        <td><a href="index.php?id={$product->getId()}&action=products/modifyProduct" class="btn bsb-btn-xl btn-light">Modify product</a></td>
     </tr>  
EOT;
    }
  } else {
    echo '<tr><td colspan="4">products not found</td></tr>';
  }
}

if ($action == "products/filterProducts") {
  if (!empty($filteredProducts)) {
    foreach ($filteredProducts as $product) {
      echo <<<EOT
    <tr>
        <td scope="row">{$product->getId()}</td>
        <td>{$product->getCode()}</a></td>
        <td>{$product->getDescription()}</td>
        <td>{$product->getPrice()}</td>
        <td><input type="text"></td>
    </tr>  
EOT;
    }
  } else {
    echo '<tr><td colspan="4">products not found</td></tr>';
  }
}
echo <<<EOT


</tbody>
</table>
</div>
EOT;


} else {
  echo "Access denied";
}
?>