<?php
$admin = false;
$registered= false;

if (isset($_SESSION["status"])) {
  if ($_SESSION['role'] == "admin") {
    $admin = true;
  }
} 
if (isset($_SESSION["status"])) {
  if ($_SESSION['role'] == "registered") {
    $registered = true;
  }
} 
?>

<!-- 
<nav>
    <ul>
        <li><a href="index.php?action=home">Home</a></li>
     
<?php
if (!isset($_SESSION["status"])) {
  echo ("<li><a href='index.php?action=login'>Login</a></li>");
}
if ($admin) {
  echo ("<li><a href='index.php?action=products/manageProducts'>Manage products</a></li>");
  echo ("<li><a href='index.php?action=orders/listOrders'>Orders list</a></li>");
}
;

if (isset($_SESSION["status"])) {

  echo ("<li><a href='index.php?action=orders/createOrder'>Create order</a></li>");
  echo ("<li><a href='index.php?action=orders/listMyOrders'>My orders</a></li>");
  echo ("<li><a href='index.php?action=orders/shoppingCart'>Shopping cart</a></li>");
}
  if (isset($_SESSION["status"])) {
  echo ("<li><a href='index.php?action=logout'>Logout</a></li>");
}


?>
    </ul>
</nav> -->

<nav class="navbar navbar-expand-lg navbar-light" style='background-color: #F2F2EC;'>

  <div class="container-fluid">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php?action=home">Home</a>
        </li>
        <?php
        if (!isset($_SESSION["status"])) {
          echo ("<li class='breadcrumb-item'><a href='index.php?action=login'>Login</a></li>");

        };
        if ($registered) {
          echo ("<li class='breadcrumb-item'><a href='index.php?action=products/listProducts'>List all products</a></li>");
          echo ("<li class='breadcrumb-item'><a href='index.php?action=orders/createOrder'>Create order</a></li>");
          echo ("<li class='breadcrumb-item'><a href='index.php?action=orders/listMyOrders'>My orders</a></li>");
          echo ("<li class='breadcrumb-item'><a href='index.php?action=orders/shoppingCart'>Shopping cart</a></li>");
        };

        if ($admin) {
          echo ("<li class='breadcrumb-item'><a href='index.php?action=products/manageProducts'>Manage products</a></li>");
          echo ("<li class='breadcrumb-item'><a href='index.php?action=orders/listOrders'>Orders list</a></li>");
        };
        if (isset($_SESSION["status"])) {
          
 
          echo ("<li class='breadcrumb-item active' aria-current='page'><a href='index.php?action=logout'>Log out</a></li>");
        }


        ?>
      </ol>
    </nav>
  </div>
</nav>