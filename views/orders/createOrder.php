<?php //echo ("en create order");
$products = $params['products'] ?? null;
$filteredProducts = $params['filteredProducts'] ?? null;
$action = $params['action']?? null;
$message = $params['message'] ?? null;


if (isset($_SESSION["status"])) {
  if ($_SESSION['role'] == "registered") {




if ($message != null) {
    echo <<<EOT
    <p class="text-decoration-underline text-danger">$message</p>
    EOT;
  }


echo <<<EOT


<form action="index.php" method="post">
<div class="row gy-3 gy-md-4 ms-5 overflow-hidden">
  <div class="col-3">
    <label for="descriptionInput" class="form-label">Product search</label>
    <input type="text" class="form-control" name="descriptionInput" id="descriptionInput"  maxlength="10"  required>
  </div>  
  <div class="col-3">
    <div class="d-grid">
<p></p><p></p>
      <button class="btn bsb-btn-xl btn-light" type="submit" id="button" name = "action" value="orders/searchProducts">search</button>
    </div>
  </div>
</div>
<div class="row gy-3 gy-md-4 overflow-hidden">

  </div>
</form>
<br>
<br>


EOT;



if ($action == "orders/searchProducts") {

    echo <<<EOT
    <form action="index.php" method="post">
        <div class="row gy-3 gy-md-4 ms-5 me-5 overflow-hidden justify-content-center">
        <table class="table ">
        <thead>
            <tr>
            <th scope="col">Id</th>
            <th scope="col">Code</th>
            <th scope="col">Description</th>
            <th scope="col">Price</th>
            <th scope="col">Quantity</th>
            <th scope="col"></th>
            <th scope="col"></th>
            <th scope="col"></th>
            

            </tr>
        </thead>
        <tbody>

  EOT;

    if (!empty($filteredProducts)) {
        foreach ($filteredProducts as $product) {
            echo <<<EOT
            <form action="index.php" method="post">
    <tr>
        <td scope="row">{$product->getId()}</td>
        <td>{$product->getCode()}</a></td>
        <td>{$product->getDescription()}</td>
        <td>{$product->getPrice()}</td>
        <td><input type="text" class="form-control small-input" name="productQuantity" id="productQuantity"  maxlength="2"  ></td>
           <td><button class="btn bsb-btn-xl btn-light mb-2" type="submit" id="button" name="action" value="orders/addProductToOrder">Add</button></td>
           <td><input type="hidden" name="price" value="{$product->getPrice()}"></td>
           <td><input type="hidden" name="id" value="{$product->getId()}"></td>
    </tr>  
    </form>
EOT;
        }
    } else {
        echo '<tr><td colspan="4">products not found</td></tr>';
    }
}
echo <<<EOT

</tbody>
</table>
</div>

EOT;

}
} else {
  echo "Access denied";
}