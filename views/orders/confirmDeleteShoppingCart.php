<?php

if (isset($_SESSION["status"])) {
    if ($_SESSION['role'] == "registered") {
    echo <<<EOT
    
    <div class="ms-5">
        <p>Are you sure you want to delete your shopping cart?</p>
        </div>
        <form action="index.php" method="get">
        <div class="ms-5">
            <button class="btn bsb-btn-xl btn-light" type="submit" id="button" name = "action" value="orders/deleteShoppingCart">Yes</button>
        </div>
        <p></p>
        <div class="ms-5">
            <a href="index.php?action=orders/shoppingCart" class="btn bsb-btn-xl btn-dark mb-2">No</a>
            </div>
        </form>
EOT;
}
} else {
    echo "Access denied";
  }

?>
