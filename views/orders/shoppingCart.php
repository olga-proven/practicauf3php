<?php //echo ("en shopping cart");
$model = new Model();

if (isset($_SESSION["status"])) {
    if ($_SESSION['role'] == "registered") {
        $user = $params['user']?? null;
        $orderItems = $params['orderItems']?? null;
        $message = $params['message'] ?? null;

        echo <<<EOT
        <div class="ms-5 overflow-hidden">
   
        <p>Hola, {$user->getUsername()}!</p>
       
        <p>Your email is {$user->getEmail()}</p>

        </div>
        <p class="text-decoration-underline text-danger" >$message</p>

     
        EOT;

        if ($orderItems) {
            echo <<<EOT
            <form action="index.php" method="post">
            <div class="row gy-3 gy-md-4 ms-5 me-5 overflow-hidden">
            <table class="table ">
            <thead>
                <tr>
                <th scope="col">Id</th>
                <th scope="col">Description</th>      
                <th scope="col">Price</th>
                <th scope="col">Quantity</th>
                <th scope="col">Total price</th>
                </tr>
            </thead>
            <tbody>
            EOT;
        
            foreach ($orderItems as $orderItem) {
                //print_r($orderItem);
                //echo ("<br>");
                
                $productId = $orderItem->getProductId();
                $product = $model->searchProductById($productId);
                $totalPrice = $product->getPrice() * $orderItem->getQuantity();
                
                echo <<<EOT
                <tr>
                    <td scope="row">{$orderItem->getProductId()}</td>
                    <td>{$product->getDescription()}</td>
                    <td>{$product->getPrice()}</td>
                    <td>{$orderItem->getQuantity()}</td>
                    <td>{$totalPrice}</td>
                </tr>  
                EOT;
            }
        
            echo <<<EOT
            </tbody>
            </table>
            </div>
            EOT;
        
            echo <<<EOT
            <div class="ms-5">
            <p>Choose delivery method:</p>
            <select class="btn bsb-btn-xl btn-light mb-2" name="select">
                <option value="Click & Collect">Click&Collect</option>
                <option value="Home delivery">Home delivery</option>
            </select>
            <p></p>
            <button class="btn bsb-btn-xl btn-dark" type="submit" id="button" name="action" value="orders/createOrderInDatabase">Buy</button>
            <button class="btn bsb-btn-xl btn-dark" type="submit" id="button" name="action" value="orders/confirmDeleteShoppingCart">Cancel</button>
            <p></p>
            <a href="index.php?action=orders/createOrder" class="btn bsb-btn-xl btn-dark mb-2">Continue shopping</a>
            </div>
            </form>
            EOT;
        }
        

    }

 else {
  echo "Access denied";
}}