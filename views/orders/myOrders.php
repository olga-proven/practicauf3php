<?php 
$model = new Model();

if (isset($_SESSION["status"])) {
    if ($_SESSION['role'] == "registered") {
        $message = $params['message'] ?? null;
        $orders = $params['orders'] ?? null;

    if(!empty($message)){
    echo <<<EOT

        <p class="text-decoration-underline text-danger" >$message</p>
        EOT;
    }
    else{
        echo <<<EOT

        
        <div class="row gy-3 gy-md-4 ms-5 me-5 overflow-hidden">
        <table class="table ">
        <thead>
            <tr>
            <th scope="col">Id</th>
            <th scope="col">Date</th>
            <th scope="col">Delivery method</th>
                  

            </tr>
        </thead>
        <tbody>
        EOT;

        foreach ($orders as $order) {
            echo <<<EOT
                    <tr>
                <td scope="row">{$order->getId()}</td>
                <td>{$order->getDate()->format('Y-m-d H:i:s')}</a></td>
                <td>{$order->getDeliveryMethod()}</td>
                
                
            </tr>  
            EOT;

    }    

                echo <<<EOT

            </tbody>
            </table>
            </div>
            EOT;

    }   }

    else {
     echo "Access denied";
   }}