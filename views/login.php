
<?php


$mensaje = $params['result']??null;
$username = $params['username']??null;
$password = $params['password']??null;


if (!isset($_SESSION["status"])) { 
echo <<<EOT



<!-- Login 3 - Bootstrap Brain Component -->
<section class="p-3 p-md-4 p-xl-5">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-6 bsb-tpl-bg-platinum">
        <div class="d-flex flex-column justify-content-between h-100 p-3 p-md-4 p-xl-5">
          <!-- <h3 class="m-0">Welcome!</h3>
          <img class="img-fluid rounded mx-auto my-4" loading="lazy" src="./assets/img/bsb-logo.svg" width="245" height="80" alt="BootstrapBrain Logo">
          <p class="mb-0">Not a member yet? <a href="#!" class="link-secondary text-decoration-none">Register now</a></p> -->
        </div>
      </div>
      <div class="col-12 col-md-6 bsb-tpl-bg-lotion">
        <div class="p-3 p-md-4 p-xl-5">
          <div class="row">
            <div class="col-12">
              <div class="mb-5">
                <h3>Log in</h3>
              </div>
            </div>
          </div>
          <form action="index.php" method="post">
            <div class="row gy-3 gy-md-4 overflow-hidden">
              <div class="col-12">
                <label for="username" class="form-label">Username <span class="text-danger">*</span></label>
                <input type="username" class="form-control" name="username" id="username" value="$username" maxlength="10"  required>
              </div>
              <div class="col-12">
                <label for="password" class="form-label">Password <span class="text-danger">*</span></label>
                <input type="password" class="form-control" name="password" id="password" value="$password" maxlength="10" required>
              </div>

              <div class="col-12">
                <div class="d-grid">
                  <button class="btn bsb-btn-xl btn-primary" type="submit" id="button" name = "action" value="doLogin">Log in now</button>
                </div>
              </div>
            </div>
          </form>
 <div class="row">
            <div class="col-12">
              <hr class="mt-5 mb-4 border-secondary-subtle">
              <div class="text-end">
                <!-- <a href="#!" class="link-secondary text-decoration-none">Forgot password</a -->
                <p class="link-secondary text-decoration-none">{$mensaje}</p>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>


EOT;



}

else{
  
  echo <<<EOT


  <!-- Login 3 - Bootstrap Brain Component -->
  <section class="p-3 p-md-4 p-xl-5">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-6 bsb-tpl-bg-platinum">
          <div class="d-flex flex-column justify-content-between h-100 p-3 p-md-4 p-xl-5">
            <!-- <h3 class="m-0">Welcome!</h3>
            <img class="img-fluid rounded mx-auto my-4" loading="lazy" src="./assets/img/bsb-logo.svg" width="245" height="80" alt="BootstrapBrain Logo">
            <p class="mb-0">Not a member yet? <a href="#!" class="link-secondary text-decoration-none">Register now</a></p> -->
          </div>
        </div>
        <div class="col-12 col-md-6 bsb-tpl-bg-lotion">
          <div class="p-3 p-md-4 p-xl-5">
            <div class="row">
              <div class="col-12">
                <div class="mb-5">
                  <h3>You've already logged in</h3>
                </div>
              </div>
            </div>
            <form action="index.php" method="post">
              <div class="row gy-3 gy-md-4 overflow-hidden">
                <div class="col-12">
                  <label for="username" class="form-label">Username <span class="text-danger">*</span></label>
                  <input type="username" class="form-control" name="username" id="username" value="$username" maxlength="10"  required>
                </div>
                <div class="col-12">
                  <label for="password" class="form-label">Password <span class="text-danger">*</span></label>
                  <input type="password" class="form-control" name="password" id="password" value="$password" maxlength="10" required>
                </div>

                <div class="col-12">
                  <div class="d-grid">
                    <button class="btn bsb-btn-xl btn-primary" type="submit" id="button" name = "action" value="doLogin" disabled>Log in now</button>
                  </div>
                </div>
              </div>
            </form>
   <div class="row">
              <div class="col-12">
                <hr class="mt-5 mb-4 border-secondary-subtle">
                <div class="text-end">
                  <!-- <a href="#!" class="link-secondary text-decoration-none">Forgot password</a -->
                  <p class="link-secondary text-decoration-none">{$mensaje}</p>
                </div>
              </div>
            </div>
  
          </div>
        </div>
      </div>
    </div>
  </section>
  
  
  EOT;

}
?>
