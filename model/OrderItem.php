<?php

class OrderItem {
    private ?int $id;
    private int $productId;
    private int $quantity;
    private float $itemPrice;

    public function __construct(?int $id, int $productId, int $quantity, float $itemPrice) {
        $this->id = $id;
        $this->productId = $productId;
        $this->quantity = $quantity;
        $this->itemPrice = $itemPrice;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function setId(?int $id) {
        $this->id = $id;
    }

    public function getProductId(): int {
        return $this->productId;
    }

    public function setProductId(int $productId) {
        $this->productId = $productId;
    }

    public function getQuantity(): int {
        return $this->quantity;
    }

    public function setQuantity(int $quantity) {
        $this->quantity = $quantity;
    }

    public function getItemPrice(): float {
        return $this->itemPrice;
    }

    public function setItemPrice(float $itemPrice) {
        $this->itemPrice = $itemPrice;
    }

    public function __toString() {
        return "OrderItem{id=$this->id,productId=$this->productId,quantity=$this->quantity,itemPrice=$this->itemPrice}";
    }
}
