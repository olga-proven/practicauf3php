<?php

class Order {
    private int $id;
    private DateTime $date;
    private string $deliveryMethod;
    private int $customer;

    public function __construct(int $id, DateTime $date, string $deliveryMethod, int $customer) {
        $this->id = $id;
        $this->date = $date;
        $this->deliveryMethod = $deliveryMethod;
        $this->customer = $customer;
    }

    public function getId(): int {
        return $this->id;
    }

    public function setId(int $id) {
        $this->id = $id;
    }

    public function getDate(): DateTime {
        return $this->date;
    }

    public function setDate(DateTime $date) {
        $this->date = $date;
    }

    public function getDeliveryMethod(): string {
        return $this->deliveryMethod;
    }

    public function setDeliveryMethod(string $deliveryMethod) {
        $this->deliveryMethod = $deliveryMethod;
    }

    public function getCustomer(): int {
        return $this->customer;
    }

    public function setCustomer(int $customer) {
        $this->customer = $customer;
    }

    public function __toString() {
        return "Order{id=$this->id,date=" . $this->date->format('Y-m-d H:i:s') . ",deliveryMethod=$this->deliveryMethod,customer=$this->customer}";
    }
}
