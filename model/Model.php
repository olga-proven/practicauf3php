<?php
require_once 'User.php';
require_once 'persist/UserPdoDbDao.php';

require_once 'Product.php';
require_once 'persist/ProductPdoDbDao.php';

require_once 'Order.php';
require_once 'persist/OrderPdoDbDao.php';

require_once 'OrderItem.php';
require_once 'persist/OrderItemPdoDbDao.php';

require_once 'lib/DbConnect.php';

class Model
{
    private $UserDataSource;

    private $OrderDataSource;

    private $OrderItemDataSource;

    private $ProductDataSource;

    public function __construct()
    {
        //$this->dataSource = ItemArrayDao::getInstance();
        $this->UserDataSource = UserPdoDbDao::getInstance();

        $this->OrderItemDataSource = OrderItemPdoDbDao::getInstance();

        $this->ProductDataSource = ProductPdoDbDao::getInstance();

        $this->OrderDataSource = OrderPdoDbDao::getInstance();


    }

    /*   USER FUNCTIONS */

    /**
     * finds all users.
     * @return array list of USERS object or empty array if none found.
     */
    public function selectAllUsers()
    {

        $users = $this->UserDataSource->selectAll();
        return $users;
    }

    /**
     * finds user by username and password.
     * @return object user  if exists info or null if none found.
     */
    public function searchUsernamePassword(string $searchUsername, string $searchPassword)
    {

        $user = $this->UserDataSource->searchUsernamePassword($searchUsername, $searchPassword);
        return $user;
    }

    /**
     * finds user by username 
     * @return object user  if exists info or null if none found.
     */
    public function searchUsername(string $searchUsername)
    {

        $user = $this->UserDataSource->searchUsername($searchUsername);
        return $user;
    }



    /**
     * Insert user info in the database if user no exists
     * @param object User 
     *  @return int 1 if user inserted and 0 if not 
     */

    public function addUser(object $user): int
    {
        $result = $this->UserDataSource->addUser($user);
        return $result;
    }


    /**
     * Delete user from the database.
     * @param string $username The username of the user to be deleted.
     * @return int 1 if user deleted, 0 if not found or error occurred.
     */

    public function deleteUser(string $username): int
    {
        $result = $this->UserDataSource->deleteUser($username);
        return $result;
    }


    /**
     * Update user info in the database.
     * @param object $user User object with updated information.
     * @return int 1 if user updated, 0 if not found or error occurred.
     */

    public function updateUser(object $user): int
    {
        $result = $this->UserDataSource->updateUser($user);
        return $result;
    }

    /*   PRODUCT FUNCTIONS */


    /**
     * finds all products.
     * @return array list of products object or empty array if none found.
     */
    public function selectAllProducts(): array
    {

        $products = $this->ProductDataSource->selectAll();
        return $products;
    }

    /**
     * Search list of the products which description contains the  description provided
     * @param string $searchByDescription - description to search
     *  @return array  if the products objects  o array vacio
     */
    public function searchByDescription(string $searchDescription): array
    {

        $products = $this->ProductDataSource->searchByDescription($searchDescription);
        return $products;
    }

    /**
     * finds product by ID
     * @return object Product if exists info or null if none found.
     */
    public function searchProductById(int $searchId): ?Product
    {

        $product = $this->ProductDataSource->searchById($searchId);
        return $product;
    }

    /**
     * finds product by Code
     * @return object Product if exists info or null if none found.
     */
    public function searchProductByCode(string $searchCode): ?Product
    {

        $product = $this->ProductDataSource->searchByCode($searchCode);
        return $product;
    }


    /**
     * Insert product info in the database if product no exists
     * @param object Product
     *  @return int 1 if product inserted and 0 if not 
     */

    public function addProduct(object $product): int
    {
        $result = $this->ProductDataSource->addProduct($product);
        return $result;
    }


    /**
     * Delete product from the database.
     * @param int $productId The productId of the product to be deleted.
     * @return int 1 if product deleted, 0 if not found or error occurred.
     */

    public function deleteProduct(string $productId): int
    {
        $result = $this->ProductDataSource->deleteProduct($productId);
        return $result;
    }


    /**
     * Update product info in the database.
     * @param object $product Product object with updated information.
     * @return int 1 if product was updated, 0 if not found or error occurred.
     */

    public function modifyProduct(object $product): int
    {
        $result = $this->ProductDataSource->modifyProduct($product);
        return $result;
    }



    /**
     * Validate and modify product data.
     *
     * @param int $id Product ID.
     * @param string $code Product code.
     * @param string $description Product description.
     * @param string $price Product price.
     *
     * @return int
     *   - 1 if all data is valid .
     *   - 2 if code already exists.
     *   - 3 if price is not numeric.
     *   - 4 if description length exceeds 100 characters.
     *   - 5 if code and description are not valid.
     *   - 6 if description and price are not valid.
     *   - 7 if price and code are not valid.
     *  *   - 8 if none of the data is valid.
     */
    public function validateModifyProductData(int $id, string $code, string $description, string $price): int
    {
        $result = $this->ProductDataSource->validateModifyProductData($id, $code, $description, $price);
        return $result;
    }

    //ORDER FUNCTIONS

    /**
     * Create an order in the database.
     *
     * @param int    $customer ID of the customer.
     * @param string $deliveryMethod The delivery method for the order.
     *
     * @return int Returns 1 if inserted successfully, 0 otherwise.
     */




    private function createOrderinDatabase(int $customer, string $deliveryMethod): int
    {

        $result = $this->OrderDataSource->createOrderinDatabase($customer, $deliveryMethod);
        return $result;
    }

    /**
     * Add an order item to the database.
     *
     * @param int $orderId ID of the order.
     * @param int $productId ID of the product.
     * @param int $quantity Quantity of the product.
     * @param float $unitPrice Unit price of the product.
     *
     * @return int Returns 1 if added successfully, 0 otherwise.
     */


    private function addOrderItem(int $orderId, int $productId, int $quantity, float $unitPrice): int
    {
        $result = $this->OrderItemDataSource->addOrderItem($orderId, $productId, $quantity, $unitPrice);
        return $result;
    }
    
    /**
     * Group order items by product ID.
     *
     * @param array $orderItems Array of OrderItem objects.
     *
     * @return array An associative array where keys are product IDs and values are arrays of OrderItem objects grouped by product ID.
     */


    public function groupOrderItemsByProductId($orderItems)
    {
        $groupedItems = [];

        foreach ($orderItems as $orderItem) {
            $productId = $orderItem->getProductId();

            if (!isset($groupedItems[$productId])) {
                $groupedItems[$productId] = $orderItem;
            } else {
                $groupedItems[$productId]->setQuantity($groupedItems[$productId]->getQuantity() + $orderItem->getQuantity());
            }
        }

        return $groupedItems;
    }
    /**
     * Create an order with items in the database.
     *
     * @param int $customerId ID of the customer.
     * @param string $deliveryMethod The delivery method for the order.
     * @param array $orderItems Array of OrderItem objects.
     *
     * @return int Returns the ID of the created order if successful, otherwise returns 0.
     */


    public function createOrderWithItems(int $customerId, string $deliveryMethod, array $orderItems): int
    {
        $db = DbConnect::getInstance();
        $db->initTransaction();
        // echo("customer id");
        //echo($customerId);
        // echo("delivery method");
        // echo($deliveryMethod);
        // echo("orderItems");
        // print_r($orderItems);


        try {

            $orderId = $this->OrderDataSource->createOrderinDatabase($customerId, $deliveryMethod);

            if ($orderId > 0) {

                foreach ($orderItems as $item) {
                    $productId = $item->getProductId();
                    $quantity = $item->getQuantity();
                    $itemPrice = $item->getItemPrice();

                    $result = $this->OrderItemDataSource->addOrderItem($orderId, $productId, $quantity, $itemPrice);
                    if ($result === 0) {

                        $db->endTransaction('ROLLBACK');
                        return 0;
                    }
                }

                $db->endTransaction('COMMIT');
                return $orderId;
            } else {

                $db->endTransaction('ROLLBACK');
                return 0;
            }
        } catch (PDOException $e) {

            $db->endTransaction('ROLLBACK');
            return 0;
        }
    }

    /**
     * Search for orders associated with the specified customer ID.
     *
     * @param int $customerId The ID of the customer whose orders are to be searched.
     * @return array An array containing the orders associated with the specified customer.
     */
    public function searchUserOrders(int $customerId): array
    {
        $orders = $this->OrderDataSource->searchUserOrders($customerId);
        return $orders;
    }

        /**
     * Search for all orders.
     *
     * @return array An array containing all orders stored in the system.
     */

    public function searchAllOrders(): array
    {
        $orders = $this->OrderDataSource->searchAllOrders();
        return $orders;
    }
}


