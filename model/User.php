<?php   

class User {
    private int $id;
    private string $username;
    private string $password;
    private string $role;
    private string $email;
    private ?DateTime $birthdate;

    public function __construct(int $id, string $username, string $password, string $role,string $email, $birthdate) {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->role = $role;
        $this->email = $email;
        if ($birthdate !== null) {
            $this->birthdate = new DateTime($birthdate);
        } else {
            $this->birthdate = null;
        }
    }
 
    public function getId(): int {
        return $this->id;
    }
    
    public function setId(int $id) {
        $this->id = $id;
    }
    
    public function getUsername(): ?string {
        return $this->username;
    }    
    
    public function setUsername(?string $username) {
        $this->username = $username;
    }
    
    public function getPassword(): ?string {
        return $this->password;
    }

    public function setPassword(?string $password) {
        $this->password = $password;
    }

    public function getRole(): ?string {
        return $this->role;
    }

    public function setRole(?string $role) {
        $this->role = $role;
    }

    public function getEmail(): ?string {
        return $this->email;
    }

    public function setEmail(?string $email) {
        $this->email = $email;
    }

    public function getBirthdate(): ?DateTime {
        return $this->birthdate;
    }

    public function setBirthdate(?DateTime $birthdate) {
        if ($birthdate !== null) {
            $this->birthdate = $birthdate;
        } else {
            $this->birthdate = null;
        }
    }
    public function __toString() {
        return "User{id=$this->id,username=$this->username,password=$this->password,role=$this->role,email=$this->email, birthdate=$this->birthdate}";
    }

      
}