<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
require_once 'model/User.php';
require_once 'lib/UserPdoDb.php';


class UserPdoDbDao
{

    private static $instance = null;
    private $connection;

    private function __construct()
    {
        try {
            //PDO object creation.
            $this->connection = (new UserPdoDb())->getConnection();

        } catch (PdoException $e) {
            print "Error Code <br>" . $e->getCode();
            print "Error Message <br>" . $e->getMessage();
            print "Strack Trace <br>" . nl2br($e->getTraceAsString());
        }

    }

    /**
     * Singleton implementation of user DAO.
     * perfoms persistance in session.
     * @return UserPdoDbDao the single instance of this object.
     */
    public static function getInstance()
    {
        //        //create instance and test data only if not stored in sessin yet.
//        if (isset($_SESSION['itemDao'])) {
//            self::$instance = $_SESSION['itemDao'];
//        } else {
//            self::$instance = new self();
//            $_SESSION['itemDao'] = self::$instance;
//        }
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Search all users from data base  
     *  @return array with user objects / Null if there are no users en database 
     */
    public function selectAll(): array
    {
        $data = array();
        try {
            $statement = $this->connection->prepare('SELECT * FROM users');
            $success = $statement->execute(); //bool
            if ($success) {
                if ($statement->rowCount() > 0) {
                    $statementResult = $statement->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($statementResult as $userData) {
                        $user = $this->createUser($userData);
                        /*         print_r($user);
                                echo "<br>";
                                echo "<br>"; */
                        array_push($data, $user);
                    }
                } else {
                    $data = array();
                }
            } else {
                $data = array();
            }
        } catch (PDOException $e) {
        }
        return $data;
    }


    /**
     * Search $username and $password from data base if exists
     * @param string $searchUsername - username to search
     * @param string $searchPassword- password to search
     *  @return User if the username + password exist in data base/ Null if dont´t exist
     */

    public function searchUsernamePassword(string $searchUsername, string $searchPassword): ?User
    {
        $user = null;
        try {
            $statement = $this->connection->prepare("SELECT * FROM users WHERE username = :username and password = :password");
            $statement->bindParam(':username', $searchUsername, PDO::PARAM_STR);
            $statement->bindParam(':password', $searchPassword, PDO::PARAM_STR);
            $success = $statement->execute();
            if ($success) {
                if ($statement->rowCount() > 0) {
                    $userData = $statement->fetch(PDO::FETCH_ASSOC);
                    //echo("Resultado de bd");
                    //print_r($userData);
                    $user = $this->createUser($userData);
                } else {
                    $user = null;
                }
            } else {
                $user = null;
            }
        } catch (PDOException $e) {
            //echo $e->getTraceAsString();
        }
        return $user;
    }

    /**
     * Creates User object fron array of the user info
     * @param array $arrayUser -line fron data base with the information about user
     *  @return User object
     */

    public function createUser($arrayUser): User
    {
        $user = new User(
            intval(($arrayUser["id"])),
            $arrayUser["username"],
            $arrayUser["password"],
            $arrayUser["role"],
            $arrayUser["email"],
            $arrayUser["birthdate"]
        );
        return $user;
    }


    /**
     * Converts User object to an associative array.
     * @param object $user The User object.
     * @return array An associative array representation of the User object.
     */
    function userToArray(User $user): array
    {
        $userData = [
            "id" => $user->getId(),
            "username" => $user->getUsername(),
            "password" => $user->getPassword(),
            "role" => $user->getRole(),
            "email" => $user->getEmail(),
            "birthdate" => $user->getBirthdate() !== null ? $user->getBirthdate()->format('Y-m-d') : null,
        ];

        return $userData;
    }




    /**
     * Checks if the provided role is valid.
     * @param string $role The role to check.
     * @return bool Returns true if the role is valid ('admin' or 'registered'), false otherwise.
     */
    function checkRole(string $role): bool
    {
        $allowedRoles = ['admin', 'registered'];
        return in_array($role, $allowedRoles);
    }

    /**
     * Search $username  from data base if exists
     * @param string $searchUsername - username to search
     *  @return User if the username exist in data base/ Null if dont´t exist
     */

    public function searchUsername(string $searchUsername): ?User
    {
        $user = null;
        try {
            $statement = $this->connection->prepare("SELECT * FROM users  WHERE username = :username");
            $statement->bindParam(':username', $searchUsername, PDO::PARAM_STR);
            $success = $statement->execute();
            if ($success) {
                if ($statement->rowCount() > 0) {
                    $userData = $statement->fetch(PDO::FETCH_ASSOC);
                    //print_r($userData);
                    $user = $this->createUser($userData);
                } else {
                    $user = null;
                }
            } else {
                $user = null;
            }
        } catch (PDOException $e) {
            //echo $e->getTraceAsString();
        }
        return $user;
    }


    /**
     * Insert user info in the database if user no exists
     * @param object User 
     *  @return int 1 if user inserted and 0 if not 
     */

    public function addUser(object $user): int
    {
        $result = 0;
        $checkUsernameResult = $this->searchUsername($user->getUsername());
        //echo ("check username result");
        //var_dump($checkUsernameResult);
        //echo "<br>";
        $checkRoleResult = $this->checkRole($user->getRole());
        //echo ("check role result");
        //print_r($checkRoleResult);
        //echo "<br>";

        if (($checkUsernameResult === null) && $checkRoleResult) {
            //echo ("username doestn exist and role ok");
            //$userData= $this->userToArray($user);
            $username = $user->getUsername();
            $password = $user->getPassword();
            $role = $user->getRole();
            $email = $user->getEmail();
            $birthdate = $user->getBirthdate() !== null ? $user->getBirthdate()->format('Y-m-d') : null;
            try {
                $statement = $this->connection->prepare("INSERT INTO users (username, password, role, email, birthdate) VALUES 
                    (:username, :password, :role, :email, :birthdate)");
                $statement->bindParam(':username', $username, PDO::PARAM_STR);
                $statement->bindParam(':password', $password, PDO::PARAM_STR);
                $statement->bindParam(':role', $role, PDO::PARAM_STR);
                $statement->bindParam(':email', $email, PDO::PARAM_STR);
                $statement->bindParam(':birthdate', $birthdate, PDO::PARAM_STR);
                $success = $statement->execute();
                if ($success) {

                    if ($statement->rowCount() === 1) {
                        $result = 1;
                        //print_r($userData);

                    } else {
                        $result = 0;
                    }
                } else {
                    $result = 0;
                }
            } catch (PDOException $e) {
                
            }
        }

        return $result;
    }

    /**
     * Delete user from the database.
     * @param string $username The username of the user to be deleted.
     * @return int 1 if user deleted, 0 if not found or error occurred.
     */
    public function deleteUser(string $username): int
    {
        $result = 0;

        try {
            $statement = $this->connection->prepare("DELETE FROM users WHERE username = :username");
            $statement->bindParam(':username', $username, PDO::PARAM_STR);
            $success = $statement->execute();
            if ($success) {
                if ($statement->rowCount() > 0) {
                    $result = 1;
                } else {
                    $result = 0;
                }
            } else {
                $result = 0;
            }
        } catch (PDOException $e) {
            $result = 0;
        }
        return $result;
    }


        /**
     * Update user info in the database.
     * @param object $user User object with updated information.
    * @return int 1 if user updated, 0 if not found or error occurred.
    */
    public function updateUser(object $user): int
    {
        $result = 0;

        try {
            $checkUsername = $this->searchUsername($user->getUsername());

            if ($checkUsername !== null) {
            
                $username = $user->getUsername();
                $password = $user->getPassword();
                $role = $user->getRole();
                $email = $user->getEmail();
                $birthdate = $user->getBirthdate() !== null ? $user->getBirthdate()->format('Y-m-d') : null;

                $statement = $this->connection->prepare("UPDATE users SET 
                    password = :password, 
                    role = :role, 
                    email = :email, 
                    birthdate = :birthdate 
                    WHERE username = :username");

                $statement->bindParam(':username', $username, PDO::PARAM_STR);
                $statement->bindParam(':password', $password, PDO::PARAM_STR);
                $statement->bindParam(':role', $role, PDO::PARAM_STR);
                $statement->bindParam(':email', $email, PDO::PARAM_STR);
                $statement->bindParam(':birthdate', $birthdate, PDO::PARAM_STR);

                $success = $statement->execute();

                if ($success) {
                    
                    $result = 1;
                } else {
                    
                    $result = 0;
                }
            } else {
                
                $result = 0;
            }
        } catch (PDOException $e) {

            $result = 0;
        }

        return $result;
    }


}