<?php

require_once 'model/Order.php';
require_once 'lib/UserPdoDb.php';
require_once 'lib/DbConnect.php';


class OrderPdoDbDao
{

    private static $instance = null;
    private $connection;
    private $db;

    private function __construct()
    {
        try {

            $db = DBConnect::getInstance();
            $this->connection = $db->getConnection(); //AQUI ES DONDE HACEMOS CAMBIO Y CONNECTAMOS CON LA UNICA INSTANCIA DE CONNECTION


            //$this->connection = (new UserPdoDb())->getConnection();

        } catch (PdoException $e) {
            print "Error Code <br>" . $e->getCode();
            print "Error Message <br>" . $e->getMessage();
            print "Strack Trace <br>" . nl2br($e->getTraceAsString());
        }

    }

    /**
     * Singleton implementation of user DAO.
     * perfoms persistance in session.
     * @return OrderPdoDbDao the single instance of this object.
     */
    public static function getInstance()
    {

        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }






    /**
     * Adds a new order to the database.
     *
     * @param int    $customerId ID of the customer placing the order.
     * @param string $deliveryMethod The delivery method chosen for the order.
     *
     * @return int The ID of the newly created order.
     */

    public function createOrderinDatabase(int $customer, string $deliveryMethod): int
    {
        $result = 0;
        try {
            $statement = $this->connection->prepare("INSERT INTO orders (delMethod, customer) VALUES (:delMethod, :customer)");
            $statement->bindParam(':delMethod', $deliveryMethod, PDO::PARAM_STR);
            $statement->bindParam(':customer', $customer, PDO::PARAM_INT);

            if ($statement->execute() && $statement->rowCount() === 1) {
                $result = $this->connection->lastInsertId();

            }
        } catch (PDOException $e) {

            return 0;
        }

        return $result;
    }



    /**
     * Retrieve orders associated with the specified customer ID from the database.
     *
     * @param int $customerId The ID of the customer whose orders are to be retrieved.
     * @return array An array containing the retrieved orders.
     */

    public function searchUserOrders(int $customerId): array
    {
        $data = array();
        try {
            $statement = $this->connection->prepare("SELECT * FROM orders WHERE customer = :customer");

            $statement->bindParam(':customer', $customerId, PDO::PARAM_INT);

            $success = $statement->execute();
            if ($success) {
                if ($statement->rowCount() > 0) {
                    $statementResult = $statement->fetchAll(PDO::FETCH_ASSOC);
                    //print_r($statementResult);
                    foreach ($statementResult as $orderData) {
                        $order = $this->createOrder($orderData);
                        /*         print_r($user);
                                echo "<br>";
                                echo "<br>"; */
                        array_push($data, $order);
                    }
                } else {
                    $data = array();
                }
            } else {
                $data = array();

            }
        } catch (PDOException $e) {

            return $data;
        }

        return $data;
    }

    /**
     * Create an Order object from the provided array containing order data.
     *
     * @param array $arrayOrder An array containing the order data.
     * @return Order The created Order object.
     */

    public function createOrder($arrayOrder): Order
    {
        $order = new Order(
            intval(($arrayOrder["id"])),
            new DateTime($arrayOrder["creationDate"]),
            $arrayOrder["delMethod"],
            $arrayOrder["customer"],

        );
        return $order;
    }



    /**
     * Retrieve all orders from the database.
     *
     * @return array An array containing all retrieved orders.
     */

    public function searchAllOrders(): array
    {
        $data = array();
        try {
            $statement = $this->connection->prepare("SELECT * FROM orders");

            $success = $statement->execute();
            if ($success) {
                if ($statement->rowCount() > 0) {
                    $statementResult = $statement->fetchAll(PDO::FETCH_ASSOC);
                    //print_r($statementResult);
                    foreach ($statementResult as $orderData) {
                        $order = $this->createOrder($orderData);

                        array_push($data, $order);
                    }
                } else {
                    $data = array();
                }
            } else {
                $data = array();

            }
        } catch (PDOException $e) {

            return $data;
        }

        return $data;
    }

}