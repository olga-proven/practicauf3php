<?php

require_once 'model/OrderItem.php';
require_once 'lib/UserPdoDb.php';
require_once 'lib/DbConnect.php';


class OrderItemPdoDbDao
{

    private static $instance = null;
    private $connection;
    private $db;

    private function __construct()
    {
        try {
            //PDO object creation.
            //$this->connection = (new UserPdoDb())->getConnection();
            $db = DBConnect::getInstance();
            $this->connection = $db->getConnection();

        } catch (PdoException $e) {
            print "Error Code <br>" . $e->getCode();
            print "Error Message <br>" . $e->getMessage();
            print "Strack Trace <br>" . nl2br($e->getTraceAsString());
        }

    }

    /**
     * Singleton implementation of user DAO.
     * perfoms persistance in session.
     * @return OrderItemPdoDbDao the single instance of this object.
     */
    public static function getInstance()
    {

        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }


   

    /**
     * Inserta la información de los elementos del pedido en la base de datos.
     * @param int $orderId ID del pedido.
     * @param int $productId ID del producto.
     * @param int $quantity Cantidad del producto.
     * @param float $unitPrice Precio unitario del producto.
     * @return int Retorna 1 si se inserta correctamente y 0 si no.
     */
   
    public function addOrderItem(int $orderId, int $productId, int $quantity, float $unitPrice): int
    {
        $result = 0;
       
            try {
                $statement = $this->connection->prepare("INSERT INTO orderitems (orderId, productId, quantity, unitPrice) VALUES 
                    (:orderId, :productId, :quantity, :unitPrice)");
                $statement->bindParam(':orderId', $orderId, PDO::PARAM_INT);
                $statement->bindParam(':productId', $productId, PDO::PARAM_INT);
                $statement->bindParam(':quantity', $quantity, PDO::PARAM_INT);
                $statement->bindParam(':unitPrice', $unitPrice, PDO::PARAM_STR);

                $success = $statement->execute();
                if ($success) {

                    if ($statement->rowCount() === 1) {
                        $result = 1;
                        

                    } else {
                        $result = 0;
                    }
                } else {
                    $result = 0;
                }
            } 
            catch (PDOException $e) {
                var_dump($e->getMessage());
                return 0;
            } 
            
        return $result;
   
        }




}