<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
require_once 'model/Product.php';
require_once 'lib/UserPdoDb.php';


class ProductPdoDbDao
{

    private static $instance = null;
    private $connection;


    private function __construct()
    {
        try {
            //PDO object creation.
            $this->connection = (new UserPdoDb())->getConnection();

        } catch (PdoException $e) {
            print "Error Code <br>" . $e->getCode();
            print "Error Message <br>" . $e->getMessage();
            print "Strack Trace <br>" . nl2br($e->getTraceAsString());
        }

    }

    /**
     * Singleton implementation of products ADO.
     * perfoms persistance in session.
     * @return ProductPdoDbDao the single instance of this object.
     */
    public static function getInstance()
    {

        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Search all products from data base  
     *  @return array with product objects / Null if there are no products en database 
     */
    public function selectAll(): array
    {
        $data = array();
        try {
            $statement = $this->connection->prepare('SELECT * FROM  products');
            $success = $statement->execute(); //bool
            if ($success) {
                if ($statement->rowCount() > 0) {
                    $statementResult = $statement->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($statementResult as $productData) {
                        $product = $this->createProduct($productData);
                        /*         print_r($user);
                                echo "<br>";
                                echo "<br>"; */
                        array_push($data, $product);
                    }
                } else {
                    $data = array();
                }
            } else {
                $data = array();
            }
        } catch (PDOException $e) {
        }
        return $data;
    }

    /**
     * Creates Product object fron array of the product info
     * @param array $arrayProduct -line from data base with the information about product
     *  @return Product object
     */

    public function createProduct($arrayProduct): Product
    {
        $product = new Product(
            intval(($arrayProduct["id"])),
            $arrayProduct["code"],
            $arrayProduct["description"],
            $arrayProduct["price"],

        );
        return $product;
    }

    /**
     * Search list of the products which description contains the  description provided
     * @param string $searchByDescription - description to search
     *  @return array  if the products objects  o array vacio
     */

    public function searchByDescription(string $searchDescription): array
    {
        $data = array();
        try {
            $statement = $this->connection->prepare("SELECT * FROM  products  WHERE description like :description ");
            $searchDescriptionLike = "%" . $searchDescription . "%";
            $statement->bindParam(':description', $searchDescriptionLike, PDO::PARAM_STR);

            $success = $statement->execute();
            if ($success) {
                if ($statement->rowCount() > 0) {
                    $statementResult = $statement->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($statementResult as $productData) {
                        $product = $this->createProduct($productData);
                        /*         print_r($product);
                                echo "<br>";
                                echo "<br>"; */
                        array_push($data, $product);
                    }
                } else {
                    $data = array();
                }
            } else {
                $data = array();
            }
        } catch (PDOException $e) {
        }
        return $data;
    }



    /**
     * Search the product by id  from data base if exists
     * @param string $searchId - id to search
     *  @return Product if the id  exist in data base/ Null if dont´t exist
     */

    public function searchById(string $searchId): ?Product
    {
        $product = null;
        try {
            $statement = $this->connection->prepare("SELECT * FROM products WHERE id = :id");
            $statement->bindParam(':id', $searchId, PDO::PARAM_STR);
            $success = $statement->execute();
            if ($success) {
                if ($statement->rowCount() > 0) {
                    $productData = $statement->fetch(PDO::FETCH_ASSOC);
                    //print_r($productData);
                    $product = $this->createProduct($productData);
                } else {
                    $product = null;
                }
            } else {
                $product = null;
            }
        } catch (PDOException $e) {
            //echo $e->getTraceAsString();
        }
        return $product;
    }

    /**
     * Search for a product by code
     * @param string $searchByCode - code to search
     * @return Product|null - Product object if found, null if no match
     */
    public function searchByCode(string $searchByCode): ?Product
    {
        $product = null;
        try {
            $statement = $this->connection->prepare("SELECT * FROM products WHERE code = :code");
            $statement->bindParam(':code', $searchByCode, PDO::PARAM_STR);

            $success = $statement->execute();
            if ($success) {
                if ($statement->rowCount() > 0) {
                    // Fetch the first row of the result set
                    $productData = $statement->fetch(PDO::FETCH_ASSOC);
                    // Create a Product object from the fetched data
                    $product = $this->createProduct($productData);
                }
                // No else block is needed here since $product is already null by default.
            } else {
                // Handle error executing the query
            }
        } catch (PDOException $e) {
            // Handle PDO exception as needed
            // For now, leaving it empty
        }
        return $product;
    }


    /**
     * Insert product info in the database if product no exists
     * @param object Product 
     *  @return int 1 if product inserted, 2 if the code already exists  and 0 if other error
     */

    public function addProduct(object $product): int
    {
        $result = 0;

        $checkCode = $this->searchByCode($product->getCode());
        //echo("check code result");
        //print_r($checkCode);

        if ($checkCode === null) {

            $code = $product->getCode();
            $description = $product->getDescription();
            $price = $product->getPrice();

            try {
                $statement = $this->connection->prepare("INSERT INTO    products (code, description, price) VALUES 
                    (:code,:description,:price)");
                $statement->bindParam(':code', $code, PDO::PARAM_STR);
                $statement->bindParam(':description', $description, PDO::PARAM_STR);
                $statement->bindParam(':price', $price, PDO::PARAM_INT);

                $success = $statement->execute();
                if ($success) {

                    if ($statement->rowCount() === 1) {
                        $result = 1;


                    } else {
                        $result = 0;
                    }
                } else {
                    $result = 0;
                }
            } catch (PDOException $e) {

            }
        } else {
            $result = 2;

        }
        //echo($result);
        return $result;
    }

    /**
     * Delete a product from the database.
     * @param string $productID The product id of the product to be deleted.
     * @return int 1 if product deleted, 0 if not found or error occurred.
     */
    public function deleteProduct(string $productId): int
    {
        $result = 0;

        try {
            $statement = $this->connection->prepare("DELETE FROM products WHERE id = :id");
            $statement->bindParam(':id', $productId, PDO::PARAM_INT);
            $success = $statement->execute();
            if ($success) {
                if ($statement->rowCount() > 0) {
                    // Product deleted successfully
                    $result = 1;
                } else {
                    // Product not found
                    $result = 0;
                }
            } else {
                // Error executing the query
                $result = 0;
            }
        } catch (PDOException $e) {
            // Handle PDO exception as needed
            // For now, leaving it empty
            $result = 0;
        }
        return $result;
    }



    /**
     * Update product info in the database.
     * @param Product $product Product object with updated information.
     * @return int 1 if product updated, 0 if not found or error occurred.
     */
    public function modifyProduct(Product $product): int
    {
        $result = 0;

        try {
            // Check if the product exists
            $checkProductId = $this->searchById($product->getId());
            //echo("check product");            echo "<br>";
            //var_dump($checkProductId);
            //echo "<br>";

            if ($checkProductId !== null) {

                $description = $product->getDescription();
                $price = $product->getPrice();
                $id = $product->getId();

                $statement = $this->connection->prepare("UPDATE products SET 
                    description = :description, 
                    price = :price 
                    WHERE id  = :id");


                $statement->bindParam(':description', $description, PDO::PARAM_STR);
                $statement->bindParam(':price', $price, PDO::PARAM_STR);
                $statement->bindParam(':id', $id, PDO::PARAM_INT);

                $success = $statement->execute();

                if ($success) {
                    // Product updated successfully
                    $result = 1;
                } else {
                    // Error updating the product
                    $result = 0;
                }


            } else {
                // Product not found
                $result = 0;
            }
        } catch (PDOException $e) {
            // Handle PDO exception as needed
            // For now, leaving it empty
            $result = 0;
        }

        return $result;
    }

    /**
     * Search for a product by both code and id
     * @param string $searchCode - code to search
     * @param int $searchId - id to search
     * @return Product|null - Product object if found, null if no match
     */
    public function searchByCodeAndId(string $searchCode, int $searchId): ?Product
    {
        $product = null;
        try {
            $query = "SELECT * FROM products WHERE code = :code AND id = :id";
            $statement = $this->connection->prepare($query);

            $statement->bindParam(':code', $searchCode, PDO::PARAM_STR);
            $statement->bindParam(':id', $searchId, PDO::PARAM_INT);

            $success = $statement->execute();
            if ($success) {
                if ($statement->rowCount() > 0) {
                    // Fetch the first row of the result set
                    $productData = $statement->fetch(PDO::FETCH_ASSOC);
                    // Create a Product object from the fetched data
                    $product = $this->createProduct($productData);
                }
            } else {
                // Handle error executing the query
            }
        } catch (PDOException $e) {
            // Handle PDO exception as needed
            // For now, leaving it empty
        }

        return $product;
    }

 /**
 * Validate and modify product data.
 *
 * @param int $id Product ID.
 * @param string $code Product code.
 * @param string $description Product description.
 * @param string $price Product price.
 *
 * @return int
 *   - 1 if all data is valid .
 *   - 2 if code already exists.
 *   - 3 if price is not numeric.
 *   - 4 if description length exceeds 100 characters.
 *   - 5 if code and description are not valid.
 *   - 6 if description and price are not valid.
 *   - 7 if price and code are not valid.
 *   - 8 if none of the data is valid.
 */
public function validateModifyProductData(int $id, string $code, string $description, string $price): int
{
    $result = 0;

    $existingProduct = $this->searchByCodeAndId($code, $id);
    $existingCode = $this->searchByCode($code);

    $validCode = $existingProduct !== null || $existingCode === null;
    $validDescription = strlen($description) <= 100;
    $validPrice = is_numeric($price);

    if ($validCode && $validDescription && $validPrice) {
        $result = 1; // all data is valid and product updated
    } elseif (!$validCode && $validDescription && $validPrice) {
        $result = 2; // code already exists
    } elseif ($validCode && $validDescription && !$validPrice) {
        $result = 3; // price is not numeric
    } elseif ($validCode && !$validDescription && $validPrice) {
        $result = 4; // description length exceeds 100 characters
    } elseif (!$validCode && !$validDescription && $validPrice) {
        $result = 5; // code and description are not valid
    } elseif ($validCode && !$validDescription && !$validPrice) {
        $result = 6; // description and price are not valid
    } elseif (!$validCode && $validDescription && !$validPrice) {
        $result = 7; // price and code are not valid
    } else {
        $result = 8; // none of the data is valid
    }

    return $result;
}


}