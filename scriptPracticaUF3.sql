CREATE USER 'olga@localhost'  IDENTIFIED BY 'hola'; 
CREATE DATABASE practicaUF3
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_general_ci;
GRANT SELECT, INSERT, UPDATE, DELETE ON practicaUF3.* TO 'olga@localhost' ;
USE practicaUF3;
CREATE TABLE users (
   id INTEGER PRIMARY KEY AUTO_INCREMENT,
   username VARCHAR(10) UNIQUE,
   password VARCHAR(10),
   role VARCHAR(15),
   email VARCHAR(15),
   birthdate DATE
) ENGINE=InnoDb;


INSERT INTO users (username, password, role, email, birthdate) VALUES
  ('olga', 'hola', 'admin', 'olga@localhost', '1985-07-04'),  
  ('user1', 'pass1', 'admin', 'user1@localhost', '1986-01-04'),
  ('user2', 'pass2', 'registered', 'user2@localhost', '1987-02-04'),
  ('user3', 'pass3', 'registered', 'user3@localhost', '1988-03-04'),
  ('user4', 'pass4', 'registered', 'user4@localhost', '1989-04-04'),
  ('user5', 'pass5', 'registered', 'user5@localhost', '1990-05-04');

CREATE TABLE products (
   id INTEGER PRIMARY KEY AUTO_INCREMENT,
   code VARCHAR (10) UNIQUE,
   description VARCHAR(100) NOT NULL,
   price FLOAT
) ENGINE=InnoDb;


INSERT INTO products (code, description, price) VALUES 
    ('P1', 'Chair 1', 11.1),
    ('P2', 'Table 2', 22.2),
    ('P3', 'Sofa 3', 33.3),
    ('P4', 'Bookshelf 4', 44.4),
    ('P5', 'Cabinet 5', 55.5),
    ('P6', 'Dresser 6', 66.6),
    ('P7', 'Desk 7', 77.7),
    ('P8', 'Wardrobe 8', 88.8),
    ('P9', 'Bench 9', 99.9),
    ('P10', 'Coffee Table 10', 110.0),
    ('P11', 'Nightstand 11', 121.1),
    ('P12', 'Sideboard 12', 132.2);


    
CREATE TABLE orders (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  creationDate DATETIME DEFAULT CURRENT_TIMESTAMP,
  delMethod VARCHAR(50) NOT NULL,
  customer INTEGER references users(id) ON DELETE restrict ON UPDATE cascade
) ENGINE=InnoDb;

CREATE TABLE orderitems (
  orderId INTEGER NOT NULL references orders(id) ON DELETE cascade ON UPDATE cascade,
  productId INTEGER NOT NULL references products(id) ON DELETE restrict ON UPDATE cascade,
  quantity INTEGER NOT NULL,
  unitPrice FLOAT NOT NULL,
  PRIMARY KEY (orderId, productId)
) ENGINE=InnoDb;




INSERT INTO orders(delMethod, customer) VALUES
('Click & Collect', 4);


INSERT INTO orderitems(orderId, productId, quantity, unitPrice) VALUES
 (1, 1, 1, 11.1),
 (1, 2, 2, 44.4),
 (1, 3, 3, 99.9);
