<?php
include_once 'model/Model.php';
include_once 'model/persist/UserPdoDbDao.php';

// Instantiate model
$model = new Model();
$test=UserPdoDbDao::getInstance();
print_r ("*****************USERS*****************");
echo "<br>";


print_r("TEST: select all [find]".PHP_EOL);
echo "<br>";
$users = $model->selectAllUsers();
var_dump($users);
foreach ($users as $user) {
    foreach ($user as $key => $value) {
        echo "$key: $value ";
    }
    echo "<br>";
}

print_r("TEST: search by username and password [find]".PHP_EOL);
echo "<br>";
$user = $model->searchUsernamePassword("user1","pass1");
print_r($user);
echo "<br>";

print_r("TEST: search by username and password [NOT find]".PHP_EOL);
echo "<br>";
$user = $model->searchUsernamePassword("user1","hola");
print_r($user);
echo "<br>";


print_r("TEST: search by username  [find]".PHP_EOL);
echo "<br>";
$user = $model->searchUsername("user2");
print_r($user);
echo "<br>";

print_r("TEST: search by username  [NOT find]".PHP_EOL);
echo "<br>";
$user = $model->searchUsername("user13");
print_r($user);
echo "<br>";


print_r("TEST: addUser  [Username no exists, role ok]".PHP_EOL);
echo "<br>";
$user = new User(0,
    'user8',
    'pass8',
    'registered',
    'user8@localhost',
    '1987-02-04'
);

$result = $model->addUser($user);

print_r($result);
echo "<br>";


print_r("TEST: addUser  [Username  exists, role ok]".PHP_EOL);
echo "<br>";
$user = new User(0,
    'user2',
    'pass10',
    'registered',
    'user10@localhost',
    '1987-02-04'
);

$result = $model->addUser($user);

print_r($result);
echo "<br>";

print_r("TEST: addUser  [Username no exists, role NO ok]".PHP_EOL);
echo "<br>";
$user = new User(0,
    'user11',
    'pass11',
    'newuser',
    'user11@localhost',
    '1987-02-04'
);

$result = $model->addUser($user);

print_r($result);
echo "<br>";


print_r("TEST: deleteUser  [Username exists]".PHP_EOL);
echo "<br>";

$result = $model->deleteUser("user10");

print_r($result);
echo "<br>";



print_r("TEST: deleteUser  [Username no exists]".PHP_EOL);
echo "<br>";
$result = $model->deleteUser("user14");

print_r($result);
echo "<br>";

print_r("TEST: updateUser  [User  exists]".PHP_EOL);
echo "<br>";
$user = new User(0,
    'user2',
    'pass2',
    'admin',
    'user2@localhost',
    '1987-02-04'
);

$result = $model->updateUser($user);

print_r($result);
echo "<br>";

print_r("TEST: updateUser  [User NO exists]".PHP_EOL);
echo "<br>";
$user = new User(0,
    'user15',
    'pass2',
    'admin',
    'user2@localhost',
    '1987-02-04'
);

$result = $model->updateUser($user);

print_r($result);
echo "<br>";