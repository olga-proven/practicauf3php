<?php
include_once 'model/Model.php';
include_once 'model/persist/ProductPdoDbDao.php';

// Instantiate model
$model = new Model();

echo "*****************PRODUCTS*****************";
echo "<br>";

// Test Case: Select all products
echo "TEST: select all products [find]".PHP_EOL;
echo "<br>";
$products = $model->selectAllProducts();
var_dump($products);
foreach ($products as $product) {
    foreach ($product as $key => $value) {
        echo "$key: $value ";
    }
}
echo "<br>";

// Test Case: Search by description (find)
echo "TEST: search by description [find]".PHP_EOL;
echo "<br>";
$product = $model->searchByDescription("1");
print_r($product);
echo "<br>";

// Test Case: Search by description (not find)
echo "TEST: search by description [not find]".PHP_EOL;
echo "<br>";
$product = $model->searchByDescription("hola");
print_r($product);
echo "<br>";

// Test Case: Search by code (find)
echo "TEST: search by code [find]".PHP_EOL;
echo "<br>";
$product = $model->searchProductByCode("P1");
print_r($product);
echo "<br>";

// Test Case: Search by code (not find)
echo "TEST: search by code [not find]".PHP_EOL;
echo "<br>";
$product = $model->searchProductByCode("P20");
print_r($product);
echo "<br>";

// Test Case 1: All data is valid
echo "TEST: validateModifyProductData [All data is valid]".PHP_EOL;
$product = $model->validateModifyProductData(1, 'P1', 'Updated Chair 1', '150.5');
print_r($product);
echo "<br>";

// Test Case 2: Code already exists
echo "TEST: validateModifyProductData [Code already exists]".PHP_EOL;
$product = $model->validateModifyProductData(1, 'P2', 'New Table 2', '200.2');
print_r($product);
echo "<br>";

// Test Case 3: Price is not numeric
echo "TEST: validateModifyProductData [Price is not numeric]".PHP_EOL;
$product = $model->validateModifyProductData(3, 'P3', 'Updated Sofa 3', 'invalidPrice');
print_r($product);
echo "<br>";

// Test Case 4: Description length exceeds 100 characters
echo "TEST: validateModifyProductData [Description length exceeds 100 characters]".PHP_EOL;
$product = $model->validateModifyProductData(4, 'P4', str_repeat('a', 101), '44.4');
print_r($product);
echo "<br>";

// Test Case 5: Code and description are not valid
echo "TEST: validateModifyProductData [Code and description are not valid]".PHP_EOL;
$product = $model->validateModifyProductData(1, 'P2', str_repeat('a', 101), '55.5');
print_r($product);
echo "<br>";

// Test Case 6: Description and price are not valid
echo "TEST: validateModifyProductData [Description and price are not valid]".PHP_EOL;
$product = $model->validateModifyProductData(6, 'P6', str_repeat('a', 101), 'invalidPrice');
print_r($product);
echo "<br>";

// Test Case 7: Price and code are not valid
echo "TEST: validateModifyProductData [Price and code are not valid]".PHP_EOL;
$product = $model->validateModifyProductData(1, 'P2', 'Valid Description', 'invalidPrice');
print_r($product);
echo "<br>";

// Test Case 8: None of the data is valid
echo "TEST: validateModifyProductData [None of the data is valid]".PHP_EOL;
$product = $model->validateModifyProductData(1, 'P2', str_repeat('a', 101), 'invalidPrice');
print_r($product);
echo "<br>";

echo "TEST ADD PRODUCT";echo "<br>";
echo "1 if product inserted, 2 if the code already exists, and 0 if other error";echo "<br>";

// Test Case: Add product (Product does not exist)
echo "TEST: addProduct [Product does not exist]".PHP_EOL;
echo "<br>";
$product = new Product(0, 'P16', 'product16', 14.9);
$result = $model->addProduct($product);
print_r($result);
echo "<br>";

// Test Case: Add product (Product code already exists)
echo "TEST: addProduct [Product code exists]".PHP_EOL;
echo "<br>";
$product = new Product(0, 'P1', 'product17', 20.1);
$result = $model->addProduct($product);
print_r($result);
echo "<br>";

echo "TEST DELETE PRODUCT";echo "<br>";
echo "1 if product has been deleted, 0 if not";echo "<br>";

// Test Case: Delete product (Product id exists)
echo "TEST: deleteProduct [Product id exists]".PHP_EOL;
echo "<br>";
$result = $model->deleteProduct("12");
print_r($result);
echo "<br>";

// Test Case: Delete product (Product id does not exist)
echo "TEST: deleteProduct [Product id does not exist]".PHP_EOL;
echo "<br>";
$result = $model->deleteProduct("4444444");
print_r($result);
echo "<br>";

echo "TEST MODIFY PRODUCT";echo "<br>";
echo "1 if product has been modified, 0 if not";echo "<br>";

// Test Case: Modify product (All data is valid)
echo "TEST: modifyProduct [All data is valid]".PHP_EOL;
$product = new Product(1, 'P1', 'Updated Chair 1', 20.1);
$result = $model->modifyProduct($product);
print_r($result);
echo "<br>";
