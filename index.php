<?php
require_once 'model/OrderItem.php';
session_start();
//session is used here to simulate data persistance (only needed for array persistance).
//require_once 'model/persist/ItemArrayDao.php';
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('error_reporting', E_ALL);
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>PracticaUF3</title>
  <!--   <link rel="stylesheet" type="text/css" href="css/styles.css"> -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<body>

  <div class="wrapper">

    <div class="content">


      <?php
      include "views/topmenu.php";
      ?>


      <?php
      //dynamic html content generated here by controller.
      require_once 'controllers/MainController.php';
      $action = filter_input(INPUT_GET, 'action');
      if (isset($_SESSION["status"]) && $action != "logout" && $action !== "login") {
        echo <<<EOT
    
      <p>Welcome to our store, {$_SESSION['username']}!</p>
    
      EOT;
        }
      $cont = new MainController();
      $cont->processRequest();








      ?>


    </div>



  </div>



</body>

</html>