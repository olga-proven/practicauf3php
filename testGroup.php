<?php

require_once 'model/OrderItem.php';

function groupOrderItemsByProductId($orderItems)
{
    $groupedItems = [];

    foreach ($orderItems as $orderItem) {
        $productId = $orderItem->getProductId();

        if (!isset($groupedItems[$productId])) {
            $groupedItems[$productId] = $orderItem;
        } else {
            $groupedItems[$productId]->setQuantity($groupedItems[$productId]->getQuantity() + $orderItem->getQuantity());
        }
    }

    return $groupedItems;
}


$orderItems = [
    new OrderItem(1, 2, 5, 2000),
    new OrderItem(2, 2, 2, 2000),
    new OrderItem(3, 3, 3, 3000),
    new OrderItem(4, 3, 1, 3000)
];

$groupedItems = groupOrderItemsByProductId($orderItems);
foreach ($groupedItems as $orderItem) {
    echo ("<br>");
    echo ("<br>");

    print_r($orderItem);
}
